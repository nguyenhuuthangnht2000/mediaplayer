package com.techja.mediaplayer;

import com.techja.mediaplayer.model.SongEntities;

public interface OnActionCallBack {
    void playSong(SongEntities song);
}
