package com.techja.mediaplayer.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.techja.mediaplayer.App;
import com.techja.mediaplayer.OnActionCallBack;
import com.techja.mediaplayer.R;
import com.techja.mediaplayer.model.SongEntities;

import java.util.List;

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.SongHolder> {
    private final List<SongEntities> mData;
    private final Context mContext;
    private final OnActionCallBack callBack;
    private SongEntities currentSong;

    public SongAdapter(List<SongEntities> mData, Context mContext, OnActionCallBack callBack) {
        this.mData = mData;
        this.mContext = mContext;
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public SongHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_song, parent, false);
        return new SongHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SongHolder holder, @SuppressLint("RecyclerView") int position) {
        SongEntities item = mData.get(position);
        holder.tvTitle.setTextColor(Color.BLACK);
        if (item.equals(currentSong)) {
            holder.tvTitle.setTextColor(App.getInstance().getResources().getColor(R.color.blue));
        }
        //holder.ivSong.setImageBitmap(item.getPhoto());
        holder.tvTitle.setText(item.getTitle());
        holder.tvAlbum.setText(item.getAlbum());
        holder.tvTitle.setTag(item);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void updateUI(SongEntities song) {
        currentSong = song;
        notifyDataSetChanged();
    }

    public class SongHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitle;
        private final TextView tvAlbum;
        // private ImageView ivSong;

        @SuppressLint("NotifyDataSetChanged")
        public SongHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvAlbum = itemView.findViewById(R.id.tv_album);
            // ivSong = itemView.findViewById(R.id.iv_song);
            itemView.setOnClickListener(view -> {
                currentSong = (SongEntities) tvTitle.getTag();
                notifyDataSetChanged();
                itemView.startAnimation(AnimationUtils.loadAnimation(App.getInstance(), R.anim.alpha));
                callBack.playSong(currentSong);
            });
        }
    }
}
