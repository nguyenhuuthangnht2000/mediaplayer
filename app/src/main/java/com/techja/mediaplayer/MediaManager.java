package com.techja.mediaplayer;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.icu.text.SimpleDateFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.techja.mediaplayer.model.SongEntities;

import java.util.ArrayList;
import java.util.List;

public class MediaManager {
    public static final int STATE_IDLE = 1;
    public static final int STATE_PLAYING = 2;
    public static final int STATE_PAUSE = 3;
    private static final String TAG = MediaManager.class.getName();
    private static MediaManager instance;
    private final MediaPlayer mediaPlayer;
    private List<SongEntities> listSong;
    private int currentIndex;
    private int mState = STATE_IDLE;


    public MediaManager() {
        //for singleton
        mediaPlayer = new MediaPlayer();
        currentIndex = 0;
    }

    public static MediaManager getInstance() {
        if (instance == null) {
            instance = new MediaManager();

        }
        return instance;
    }


    //lấy nhạc từ bộ nhớ điện thoại
    @RequiresApi(api = Build.VERSION_CODES.R)
    @SuppressLint("Recycle")
    public void loadSong() {
        try {
            Uri audioUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            //lựa chọn cột
//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
//                String[] columnArr = new String[]{
//                        MediaStore.Audio.Media._ID,
//                        MediaStore.Audio.Media.TITLE,
//                        MediaStore.Audio.Media.ALBUM,
//                        MediaStore.Audio.Media.ALBUM_ARTIST,
//                        MediaStore.Audio.Media.ARTIST,
//                        MediaStore.Audio.Media.AUTHOR,
//                        MediaStore.Audio.Media.COMPOSER,
//                        MediaStore.Audio.Media.DATA,
//                };
//            }
            // lấy hết thì dùng  String[] columnArr = null;

            //Select chọn cột
            //Where điều kiện MediaStore.Audio.Media.TITLE + "= 'Chuc ngu ngon'",
            Cursor c = App.getInstance().getContentResolver().query(audioUri,
                    null,//Select chọn cột
                    null,//Where điều kiện MediaStore.Audio.Media.TITLE + "= 'Chuc ngu ngon'",
                    null,
                    MediaStore.Audio.Media.TITLE + " ASC");//order by sắp xếp
            if (c == null) return;
            int idIndex = c.getColumnIndex(MediaStore.Audio.Media._ID);
            int titleIndex = c.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int albumIndex = c.getColumnIndex(MediaStore.Audio.Media.ALBUM);
            int artistIndex = c.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            //int authorIndex = c.getColumnIndex(MediaStore.Audio.Media.AUTHOR);
            int composerIndex = c.getColumnIndex(MediaStore.Audio.Media.COMPOSER);
            int dataIndex = c.getColumnIndex(MediaStore.Audio.Media.DATA);

            listSong = new ArrayList<>();
            c.moveToFirst();
            while (!c.isAfterLast()) {
                int id = c.getInt(idIndex);
                String title = c.getString(titleIndex);
                String album = c.getString(albumIndex);
                String artist = c.getString(artistIndex);
                //String author = c.getString(authorIndex);
                String composer = c.getString(composerIndex);
                String path = c.getString(dataIndex);

                //Bitmap photo = getImageCover(path);
                SongEntities song = new SongEntities(id + "", title, album, artist, null, composer, path, null);
                listSong.add(song);
                c.moveToNext();
            }
            //close connecttion
            c.close();
            Log.i(TAG, "List Song: " + listSong.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //    private Bitmap getImageCover(String path) {
//        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
//        byte[] rawArt;
//        Bitmap art;
//        BitmapFactory.Options bfo = new BitmapFactory.Options();
//        mmr.setDataSource(path);
//        try {
//            rawArt = mmr.getEmbeddedPicture();
//            art = BitmapFactory.decodeByteArray(rawArt, 0, rawArt.length, bfo);
//        } catch (Exception e) {
//            art = BitmapFactory.decodeResource(App.getInstance().getResources(), R.drawable.ic_song);
//        }
//
//        return art;
//    }
    public void play() {
        try {
            if (mState == STATE_IDLE) {
                mediaPlayer.reset();
                String songPath = listSong.get(currentIndex).getPath();
                mediaPlayer.setDataSource(songPath);
                mediaPlayer.prepare();
                mediaPlayer.start();
                mState = STATE_PLAYING;
            } else if (mState == STATE_PLAYING) {
                mediaPlayer.pause();
                mState = STATE_PAUSE;
            } else if (mState == STATE_PAUSE) {
                mediaPlayer.start();
                mState = STATE_PLAYING;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void next() {
        resetState();
        currentIndex++;
        if (currentIndex >= listSong.size()) {
            currentIndex = 0;
        }
        play();
    }

    public void back() {
        resetState();
        currentIndex--;
        if (currentIndex < 0) {
            currentIndex = listSong.size() - 1;
        }
        play();
    }

    public SongEntities getCurrentSong() {
        return listSong.get(currentIndex);
    }

    public void setCurrentSong(SongEntities song) {
        currentIndex = listSong.indexOf(song);

    }

    public List<SongEntities> getListSong() {
        return listSong;
    }

    public int getState() {
        return mState;
    }

    public void resetState() {
        mState = STATE_IDLE;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("SimpleDateFormat")
    public String getCurrentTime() {
        int time = mediaPlayer.getCurrentPosition();
        if (time < 0) return "00:00";
        SimpleDateFormat df;
        df = new SimpleDateFormat("mm:ss");
        return df.format(time);
    }

    public void seekTo(int progress) {
        mediaPlayer.seekTo(progress);
    }

    public int getCurrentPos() {
        return mediaPlayer.getCurrentPosition();
    }

    public int getTotalPos() {
        return mediaPlayer.getDuration();
    }

    @SuppressLint("SimpleDateFormat")
    @RequiresApi(api = Build.VERSION_CODES.N)
    public String getTotalTime() {
        int time = mediaPlayer.getDuration();
        if (time < 0) return "00:00";
        SimpleDateFormat df;
        df = new SimpleDateFormat("mm:ss");
        return df.format(time);
    }
}
