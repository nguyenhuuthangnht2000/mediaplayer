package com.techja.mediaplayer.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.techja.mediaplayer.MTask;
import com.techja.mediaplayer.MediaManager;
import com.techja.mediaplayer.OnActionCallBack;
import com.techja.mediaplayer.R;
import com.techja.mediaplayer.adapter.SongAdapter;
import com.techja.mediaplayer.model.SongEntities;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnActionCallBack, MTask.MTaskListener, SeekBar.OnSeekBarChangeListener {
    private static final String KEY_START_PLAYER = "KEY_START_PLAYER";
    private RecyclerView mRvSong;
    private TextView tvSong, tvCurrentTime, tvDuration;
    private MTask mTask;
    private SeekBar seekBar;

    private ImageView ivPlay;
    private boolean isStop;

    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    @RequiresApi(api = Build.VERSION_CODES.R)
    private void initViews() {
        mTask = new MTask(KEY_START_PLAYER, this);
        mTask.execute();

        tvSong = findViewById(R.id.tv_song);
        tvCurrentTime = findViewById(R.id.tv_current_time);
        tvDuration = findViewById(R.id.tv_duration);
        seekBar = findViewById(R.id.seek_bar);
        seekBar.setOnSeekBarChangeListener(this);

        ivPlay = findViewById(R.id.iv_play);
        ivPlay.setOnClickListener(this);

        findViewById(R.id.iv_back).setOnClickListener(this);

        findViewById(R.id.iv_next).setOnClickListener(this);


        mRvSong = findViewById(R.id.rv_song);
        mRvSong.setLayoutManager(new LinearLayoutManager(this));
        MediaManager.getInstance().loadSong();
        SongAdapter adapter = new SongAdapter(MediaManager.getInstance().getListSong(), this, this);

        mRvSong.setAdapter(adapter);

        //check permission to read storage
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            MediaManager.getInstance().loadSong();
        } else {
            requestPermissions(new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE
            }, 101);
        }
    }

    @SuppressLint("MissingSuperCall")
    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            MediaManager.getInstance().loadSong();
        }
    }


    @SuppressLint("NonConstantResourceId")
    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                MediaManager.getInstance().back();
                break;
            case R.id.iv_next:
                MediaManager.getInstance().next();
                break;
            case R.id.iv_play:
                MediaManager.getInstance().play();
                break;
            default:
                break;
        }
        updateUI();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void playSong(SongEntities song) {
        MediaManager.getInstance().resetState();
        MediaManager.getInstance().setCurrentSong(song);
        MediaManager.getInstance().play();

        updateUI();
        reset();
    }

    @SuppressLint("SetTextI18n")
    private void reset() {
        tvDuration.setText("--:--");
        tvCurrentTime.setText("00:00");
        seekBar.setMax(0);
        seekBar.setProgress(0);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void updateUI() {
        SongEntities song = MediaManager.getInstance().getCurrentSong();
        tvSong.setText(song.getTitle());

        int state = MediaManager.getInstance().getState();
        ivPlay.setImageLevel(state == MediaManager.STATE_PLAYING ? 1 : 0);
        SongAdapter adapter = (SongAdapter) mRvSong.getAdapter();
        assert adapter != null;
        adapter.updateUI(song);
        tvDuration.setText(MediaManager.getInstance().getTotalTime());
        tvCurrentTime.setText(MediaManager.getInstance().getCurrentTime());

        seekBar.setMax(MediaManager.getInstance().getTotalPos());
        seekBar.setProgress(MediaManager.getInstance().getCurrentPos());
    }

    @Override
    public void startExcute() {

    }

    @Override
    public Object excuteStart(Object dataInput, String key, MTask task) {
        isStop = false;
        while (!isStop) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (MediaManager.getInstance().getState() != MediaManager.STATE_IDLE) {
                task.requestUpdateUI(null);
            }
        }

        return null;
    }

    @Override
    protected void onDestroy() {
        isStop = true;
        mTask.cancel(true);
        super.onDestroy();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void updateUI(Object dataUpdate, String key) {
        tvCurrentTime.setText(MediaManager.getInstance().getCurrentTime());
        seekBar.setProgress(MediaManager.getInstance().getCurrentPos());
    }

    @Override
    public void completeTask(Object result, String key) {

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        MediaManager.getInstance().seekTo(seekBar.getProgress());

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}