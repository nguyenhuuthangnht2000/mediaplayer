package com.techja.mediaplayer.model;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;

import java.util.Objects;

public class SongEntities {
    private String id;
    private String title;
    private String album;
    private String artist;
    private String author;
    private String composer;
    private String path;
    private Bitmap photo;

    public SongEntities(String id, String title, String album, String artist, String author, String composer, String path, Bitmap photo) {
        this.id = id;
        this.title = title;
        this.album = album;
        this.artist = artist;
        this.author = author;
        this.composer = composer;
        this.path = path;
        this.photo = photo;
    }
    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getComposer() {
        return composer;
    }

    public void setComposer(String composer) {
        this.composer = composer;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @NonNull
    @Override
    public String toString() {
        return "\nSongEntities{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", album='" + album + '\'' +
                ", artist='" + artist + '\'' +
                ", author='" + author + '\'' +
                ", composer='" + composer + '\'' +
                ", path='" + path + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SongEntities that = (SongEntities) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, album, artist, author, composer, path, photo);
    }
}
